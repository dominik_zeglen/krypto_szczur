#include "iostream"
#include "cstdlib"
#include "ctime"
#include "string"
#include "bitset"

using namespace std;

string cipher(string information, int * key)
{
	for(int i = 0; i < information.length(); i++)
	{
		information[i] ^= key[i];
	}
	
	return information;
}

string decipher(string encrypted, int * key)
{
	for(int i = 0; i < encrypted.length(); i++)
	{
		encrypted[i] ^= key[i];
	}
	
	return encrypted;
}

int main()
{
	srand(time(NULL));
	
	string information;
	int * key;
	
	cout << "Prezentacja dzialania algorytmu szyfrowania One Time Pad.\nProsze podac tekst do zaszyfrowania:\n";
	getline(cin, information);
	key = new int[information.length()];
	for(int i = 0; i < information.length(); i++)
	{
		key[i] = rand() % 127;
	}
	
	cout << "Oto reprezentacja bitowa informacji:\n";
	
	for(int i = 0; i < information.length(); i++)
	{
		cout << bitset<7>(information[i]);
	}
	
	cout << "\nOto klucz:\n";
	
	for(int i = 0; i < information.length(); i++)
	{
		cout << bitset<7>(key[i]);
	}
	
	cout << "\nOto tekst po zaszyfrowaniu:\n";
	string encrypted = cipher(information, key);
	
	for(int i = 0; i < information.length(); i++)
	{
		cout << bitset<7>(encrypted[i]);
	}
	
	cout << "\nOto tekst po odcyfrowaniu:\n";
	cout << decipher(encrypted, key);
	
	cout << "\nNacisnij dowolny przycisk, by zakonczyc dzialanie programu\n";
	cin.get();
	
	free(key);

	return 0;
}
